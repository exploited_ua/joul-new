$(document).ready(function () {

    $('.equipment-btn').on('click', function() {
        $(this).parent().toggleClass('active');
    });
    $(document).on('click', function (e) {
        var container = $(".equipment-dropdown");
        if (container.has(e.target).length === 0) {
            container.removeClass('active');
        }
    });
    // $(document).on('click', function (e) {
    //     var close_dropdown = $(".header-button");
    //     if (close_dropdown.has(e.target).length === 0) {
    //         close_dropdown.removeClass('active');
    //     }
    // });

    $('.hamburger').on('click', function() {
        $('.header-button').toggleClass('active');
        $(this).toggleClass('close');
    });

    var swiper = new Swiper('.swiper-implemented-projects', {
        spaceBetween: 30,
        speed: 1000,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 6000,
        }
    });

    $('.select2').select2();


    $('.check-calc').on('click', function () {
        if ($(this).prop('checked')) {
            $(this).parent().next().removeClass('disabled');
        } else {
            $(this).parent().next().addClass('disabled');
        }
    });


    $('.show-password-link').hover(function () {
            $(this).prev().attr('type', 'text');
        },
        function () {
            $(this).prev().attr('type', 'password');
        });


});


$('.calc-elem').each(function () {
    $(this).keyup(function () {
        calc(this);
    })
});

function calc(elem) {
    var itemVal = [];
    var par = elem.closest('.calc-field-wrap');
    var calcItems = $(par).find('.calc-elem');
    calcItems.each(function () {
        itemVal.push($(this).val());
    });
    var result = (((itemVal[0] * itemVal[1] * itemVal[2]) / itemVal[3]) / 1000).toFixed(1);
    $(par).find('.calc-result span').text(result);
}

$('.calc-form').on('submit', function (e) {
    e.preventDefault();
    var sum = 0;
    $('.calc-result span').each(function () {
        sum += Number($(this).text());
    });
    $('.total-result span').text(sum);
});


var capacitySlider = $('#capacity-slider');
var powerStation = $('#power-station');
if ($('#power-station').length >= 1) {

    var powerMinVal = powerStation.data('min');
    var powerMaxVal = powerStation.data('max');
    noUiSlider.create(powerStation[0], {
        connect: [true, false],
        range: {
            'min': powerMinVal,
            'max': powerMaxVal
        },
        start: 0,
        tooltips: true,
        format: wNumb({
            decimals: 0,
            suffix: ' кВт'

        }),
        pips: {
            mode: 'steps',
            density: 100,
            format: wNumb({
                decimals: 0,
                suffix: ' кВт'

            }),
        }
    });


}


if ($('#capacity-slider').length >= 1) {

    var powerMinVal = capacitySlider.data('min');
    var powerMaxVal = capacitySlider.data('max');
    noUiSlider.create(capacitySlider[0], {
        connect: [true, false],
        range: {
            'min': powerMinVal,
            'max': powerMaxVal
        },
        start: 0,
        tooltips: true,
        format: wNumb({
            decimals: 0,
            suffix: ' кВт-год'

        }),
        pips: {
            mode: 'steps',
            density: 100,
            format: wNumb({
                decimals: 0,
                suffix: ' кВт-год'

            }),
        }
    });

}


powerStation[0].noUiSlider.on('update', function (values, handle) {
    var Format = wNumb({
        suffix: ' кВт'
    });
    $('.square-text').text(Format.from(values[handle]) * 7);

});


$('.show-more').on('click', function (e) {
    e.preventDefault();
    var height = $(this).prev('.hidden-text').find('p').outerHeight();
    $(this).hide();
    if (height > 250)
        $(this).prev('.hidden-text').css('height', height).addClass('visible')
});